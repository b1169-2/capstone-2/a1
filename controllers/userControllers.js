const User = require("./../models/User");
const Product = require("./../models/Product")

const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.register = (reqBody) => {
	const {email} = reqBody

	return User.findOne({email: email}).then( (result, error) => {
		if(result != null){
			return `Email already exists`
		} else {
			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
		
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				} else {
					return false
				}
			})
		}
	})
}


module.exports.getAllUsers = () => {
    
    return User.find().then( (result, error) => {
        if(result){
            return result
        } else {
            return error
        }
    })
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.setAsAdmin = (params) => {

    return User.findByIdAndUpdate(params, {isAdmin: true}).then( (result, error) => {

        if(result == null){
            return `User does not exist`
        } else {
            if(result){
                return true
            } else {
                return false
            }
        }
    })
}

module.exports.checkout = async (data) => {
	const {userId, productId} = data


	const userCheckout = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			result.orders.push({productId: productId})

			if(result.orders.productId != productId){
				return result.save().then((result, err) => {
					if(err){
						return err
					} else {
						return result
					}
				})
			} else {
				return false
			}
		}
	})

	if(userCheckout){
		return true
	} else {
		return false
	}
}

module.exports.getAllOrders = () => {

    return User.find({__v: {$gt: 0}}).then( (result, error) => {

        if(result){
            return result
        } else {
            return error
        }
    })
}

module.exports.getOrders = (data) => {
	// console.log(data)
	const {id} = data

	return User.findById(id).then((result, err) => {
		// console.log(result)

		if(result != null){
			result.password = "";
			return result.orders
		} else {
			return false
		}
	})
}