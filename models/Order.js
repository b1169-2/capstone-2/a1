const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: true
    },
    cart: [
        {
            productId: {
                type: String,
                required: true
            },
            quantity: {
                type: Number,
                default: 1
            }
        }
    ],
    status: {
        type: String,
        default: "pending"
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
});

module.exports = mongoose.model("Cart", cartSchema);