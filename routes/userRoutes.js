const express = require("express");
const router = express.Router();

const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

router.post("/email-exists", (req, res) => {

	userController.checkEmail(req.body).then( result => res.send(result))
})

//Register
router.post("/register", (req, res) => {

    userController.register(req.body).then(result => res.send(result))
})

//Get all users
router.get("/", (req, res) => {

    userController.getAllUsers().then( result => res.send(result))
})

//Login
router.post("/login", (req, res) => {

	userController.login(req.body).then(result => res.send(result))
})

//Set as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        userController.setAsAdmin(req.params.userId, userData).then(result => res.send(result))
    } else {
        res.send(false)
    }
    
})

//Orders
router.post("/addCart", auth.verify, (req, res) => {

	let userData = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	
	userController.checkout(userData).then(result => res.send(result))
})

//Retrieve All Orders
router.get("/orders", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userController.getAllOrders().then( result => res.send(result))
	} else{
		res.send(false)
	}
    
})

//Retrieve User Orders
router.get("/myOrders", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)
	userController.getOrders(userData).then(result => res.send(result))
})


module.exports = router;